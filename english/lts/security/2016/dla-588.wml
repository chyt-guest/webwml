<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Two security related problems have been found in the mongodb
package, related to logging.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6494">CVE-2016-6494</a>

  <p>World-readable .dbshell history file</p>

<li>TEMP-0833087-C5410D

  <p>Bruteforcable challenge responses in unprotected logfile</p>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2.0.6-1+deb7u1.</p>

<p>We recommend that you upgrade your mongodb packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-588.data"
# $Id: $
