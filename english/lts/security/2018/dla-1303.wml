<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several functions were extremely slow to evaluate certain inputs due to
catastrophic backtracking vulnerabilities in several regular expressions.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-7536">CVE-2018-7536</a>

    <p>The django.utils.html.urlize() function was extremely slow to evaluate
    certain inputs due to catastrophic backtracking vulnerabilities in two
    regular expressions. The urlize() function is used to implement the urlize
    and urlizetrunc template filters, which were thus vulnerable.</p>

    <p>The problematic regular expressions are replaced with parsing logic that
    behaves similarly.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-7537">CVE-2018-7537</a>

    <p>If django.utils.text.Truncator’s chars() and words() methods were passed
    the html=True argument, they were extremely slow to evaluate certain inputs
    due to a catastrophic backtracking vulnerability in a regular expression.
    The chars() and words() methods are used to implement the truncatechars_html
    and truncatewords_html template filters, which were thus vulnerable.</p>

    <p>The backtracking problem in the regular expression is fixed.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.4.22-1+deb7u4.</p>

<p>We recommend that you upgrade your python-django packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1303.data"
# $Id: $
