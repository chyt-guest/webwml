msgid ""
msgstr ""
"Project-Id-Version: Debian webwml countries\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2011-03-25 11:05+0330\n"
"Last-Translator: Behrad Eslamifar <behrad_es@yahoo.com>\n"
"Language-Team: debian-l10n-persian <debian-l10n-persian@lists.debian.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Translator: Abbas Esmaeeli Some'eh <abbas@gnu.NOSPAM.org>\n"
"X-Poedit-Language: Persian\n"
"X-Poedit-Country: IRAN, ISLAMIC REPUBLIC OF\n"
"X-Poedit-SourceCharset: utf-8\n"

#: ../../english/template/debian/countries.wml:111
msgid "United Arab Emirates"
msgstr "امارات متحده عربی"

#: ../../english/template/debian/countries.wml:114
msgid "Albania"
msgstr "آلبانی"

#: ../../english/template/debian/countries.wml:117
msgid "Armenia"
msgstr "ارمنستان"

#: ../../english/template/debian/countries.wml:120
msgid "Argentina"
msgstr "آرژانتین"

#: ../../english/template/debian/countries.wml:123
msgid "Austria"
msgstr "اتریش"

#: ../../english/template/debian/countries.wml:126
msgid "Australia"
msgstr "استرالیا"

#: ../../english/template/debian/countries.wml:129
msgid "Bosnia and Herzegovina"
msgstr "بوسنی و هرزگوین"

#: ../../english/template/debian/countries.wml:132
msgid "Bangladesh"
msgstr "بنگلادش"

#: ../../english/template/debian/countries.wml:135
msgid "Belgium"
msgstr "بلژیک"

#: ../../english/template/debian/countries.wml:138
msgid "Bulgaria"
msgstr "بلغارستان"

#: ../../english/template/debian/countries.wml:141
msgid "Brazil"
msgstr "برزیل"

#: ../../english/template/debian/countries.wml:144
msgid "Bahamas"
msgstr "باهاماس"

#: ../../english/template/debian/countries.wml:147
msgid "Belarus"
msgstr "بلاروس"

#: ../../english/template/debian/countries.wml:150
msgid "Canada"
msgstr "کانادا"

#: ../../english/template/debian/countries.wml:153
msgid "Switzerland"
msgstr "سوئیس"

#: ../../english/template/debian/countries.wml:156
msgid "Chile"
msgstr "شیلی"

#: ../../english/template/debian/countries.wml:159
msgid "China"
msgstr "چین"

#: ../../english/template/debian/countries.wml:162
msgid "Colombia"
msgstr "کلمبیا"

#: ../../english/template/debian/countries.wml:165
msgid "Costa Rica"
msgstr "کاستاریکا"

#: ../../english/template/debian/countries.wml:168
msgid "Czech Republic"
msgstr "جمهوری چک"

#: ../../english/template/debian/countries.wml:171
msgid "Germany"
msgstr "آلمان"

#: ../../english/template/debian/countries.wml:174
msgid "Denmark"
msgstr "دانمارک"

#: ../../english/template/debian/countries.wml:177
msgid "Dominican Republic"
msgstr "جمهوری دومینیکن"

#: ../../english/template/debian/countries.wml:180
msgid "Algeria"
msgstr "الجزیره"

#: ../../english/template/debian/countries.wml:183
msgid "Ecuador"
msgstr "اکوادور"

#: ../../english/template/debian/countries.wml:186
msgid "Estonia"
msgstr "استونی"

#: ../../english/template/debian/countries.wml:189
msgid "Egypt"
msgstr "مصر"

#: ../../english/template/debian/countries.wml:192
msgid "Spain"
msgstr "اسپانیا"

#: ../../english/template/debian/countries.wml:195
msgid "Ethiopia"
msgstr "اتیوپی"

#: ../../english/template/debian/countries.wml:198
msgid "Finland"
msgstr "فنلاند"

#: ../../english/template/debian/countries.wml:201
msgid "Faroe Islands"
msgstr "جزایر فارو"

#: ../../english/template/debian/countries.wml:204
msgid "France"
msgstr "فرانسه"

#: ../../english/template/debian/countries.wml:207
msgid "United Kingdom"
msgstr "انگلستان"

#: ../../english/template/debian/countries.wml:210
msgid "Grenada"
msgstr "گرینادا"

#: ../../english/template/debian/countries.wml:213
msgid "Georgia"
msgstr "گرجستان"

#: ../../english/template/debian/countries.wml:216
msgid "Greenland"
msgstr "گرینلند"

#: ../../english/template/debian/countries.wml:219
msgid "Greece"
msgstr "یونان"

#: ../../english/template/debian/countries.wml:222
msgid "Guatemala"
msgstr "گواتمالا"

#: ../../english/template/debian/countries.wml:225
msgid "Hong Kong"
msgstr "هنگ‌کنگ"

#: ../../english/template/debian/countries.wml:228
msgid "Honduras"
msgstr "هندوراس"

#: ../../english/template/debian/countries.wml:231
msgid "Croatia"
msgstr "کرواسی"

#: ../../english/template/debian/countries.wml:234
msgid "Hungary"
msgstr "مجارستان"

#: ../../english/template/debian/countries.wml:237
msgid "Indonesia"
msgstr "اندونزی"

#: ../../english/template/debian/countries.wml:240
msgid "Iran"
msgstr "ایران"

#: ../../english/template/debian/countries.wml:243
msgid "Ireland"
msgstr "ایرلند"

#: ../../english/template/debian/countries.wml:246
msgid "Israel"
msgstr "اسرائیل"

#: ../../english/template/debian/countries.wml:249
msgid "India"
msgstr "هند"

#: ../../english/template/debian/countries.wml:252
msgid "Iceland"
msgstr "ایسلند"

#: ../../english/template/debian/countries.wml:255
msgid "Italy"
msgstr "ایتالیا"

#: ../../english/template/debian/countries.wml:258
msgid "Jordan"
msgstr "اردن"

#: ../../english/template/debian/countries.wml:261
msgid "Japan"
msgstr "ژاپن"

#: ../../english/template/debian/countries.wml:264
msgid "Kenya"
msgstr "کنیا"

#: ../../english/template/debian/countries.wml:267
msgid "Korea"
msgstr "کره"

#: ../../english/template/debian/countries.wml:270
msgid "Kuwait"
msgstr "کویت"

#: ../../english/template/debian/countries.wml:273
msgid "Kazakhstan"
msgstr "قزاقستان"

#: ../../english/template/debian/countries.wml:276
msgid "Sri Lanka"
msgstr "سریلانکا"

#: ../../english/template/debian/countries.wml:279
msgid "Lithuania"
msgstr "لیتوانی"

#: ../../english/template/debian/countries.wml:282
msgid "Luxembourg"
msgstr "لوکزامبورگ"

#: ../../english/template/debian/countries.wml:285
msgid "Latvia"
msgstr "لتونی"

#: ../../english/template/debian/countries.wml:288
msgid "Morocco"
msgstr "مراکش"

#: ../../english/template/debian/countries.wml:291
msgid "Moldova"
msgstr "مالدیو"

#: ../../english/template/debian/countries.wml:294
msgid "Montenegro"
msgstr "مونته‌نگرو"

#: ../../english/template/debian/countries.wml:297
msgid "Madagascar"
msgstr "ماداگاسکار"

#: ../../english/template/debian/countries.wml:300
#, fuzzy
#| msgid "Dominican Republic"
msgid "Macedonia, Republic of"
msgstr "جمهوری دومینیکن"

#: ../../english/template/debian/countries.wml:303
msgid "Mongolia"
msgstr "مونگولیا"

#: ../../english/template/debian/countries.wml:306
msgid "Malta"
msgstr "مالتا"

#: ../../english/template/debian/countries.wml:309
msgid "Mexico"
msgstr "مکزیک"

#: ../../english/template/debian/countries.wml:312
msgid "Malaysia"
msgstr "مالزی"

#: ../../english/template/debian/countries.wml:315
msgid "New Caledonia"
msgstr "کالدونیای جدید"

#: ../../english/template/debian/countries.wml:318
msgid "Nicaragua"
msgstr "نیکاراگوئه"

#: ../../english/template/debian/countries.wml:321
msgid "Netherlands"
msgstr "هلند"

#: ../../english/template/debian/countries.wml:324
msgid "Norway"
msgstr "نروژ"

#: ../../english/template/debian/countries.wml:327
msgid "New Zealand"
msgstr "زلاند نو"

#: ../../english/template/debian/countries.wml:330
msgid "Panama"
msgstr "پاناما"

#: ../../english/template/debian/countries.wml:333
msgid "Peru"
msgstr "پرو"

#: ../../english/template/debian/countries.wml:336
msgid "French Polynesia"
msgstr "پلینزی فرانسوی"

#: ../../english/template/debian/countries.wml:339
msgid "Philippines"
msgstr "فیلیپین"

#: ../../english/template/debian/countries.wml:342
msgid "Pakistan"
msgstr "پاکستان"

#: ../../english/template/debian/countries.wml:345
msgid "Poland"
msgstr "لهستان"

#: ../../english/template/debian/countries.wml:348
msgid "Portugal"
msgstr "پرتغال"

#: ../../english/template/debian/countries.wml:351
msgid "Réunion"
msgstr ""

#: ../../english/template/debian/countries.wml:354
msgid "Romania"
msgstr "رومانی"

#: ../../english/template/debian/countries.wml:357
msgid "Serbia"
msgstr "صربستان"

#: ../../english/template/debian/countries.wml:360
msgid "Russia"
msgstr "روسیه"

#: ../../english/template/debian/countries.wml:363
msgid "Saudi Arabia"
msgstr "عربستان سعودی"

#: ../../english/template/debian/countries.wml:366
msgid "Sweden"
msgstr "سوئد"

#: ../../english/template/debian/countries.wml:369
msgid "Singapore"
msgstr "سنگاپور"

#: ../../english/template/debian/countries.wml:372
msgid "Slovenia"
msgstr "اسلوونی"

#: ../../english/template/debian/countries.wml:375
msgid "Slovakia"
msgstr "اسلواکی"

#: ../../english/template/debian/countries.wml:378
msgid "El Salvador"
msgstr "السالوادور"

#: ../../english/template/debian/countries.wml:381
msgid "Thailand"
msgstr "تایلند"

#: ../../english/template/debian/countries.wml:384
#, fuzzy
#| msgid "Pakistan"
msgid "Tajikistan"
msgstr "پاکستان"

#: ../../english/template/debian/countries.wml:387
msgid "Tunisia"
msgstr "تونس"

#: ../../english/template/debian/countries.wml:390
msgid "Turkey"
msgstr "ترکیه"

#: ../../english/template/debian/countries.wml:393
msgid "Taiwan"
msgstr "تایوان"

#: ../../english/template/debian/countries.wml:396
msgid "Ukraine"
msgstr "اکراین"

#: ../../english/template/debian/countries.wml:399
msgid "United States"
msgstr "ایالات متحده"

#: ../../english/template/debian/countries.wml:402
msgid "Uruguay"
msgstr "اروگوئه"

#: ../../english/template/debian/countries.wml:405
msgid "Uzbekistan"
msgstr "ازبکستان"

#: ../../english/template/debian/countries.wml:408
msgid "Venezuela"
msgstr "ونزوئلا"

#: ../../english/template/debian/countries.wml:411
msgid "Vietnam"
msgstr "ویتنام"

#: ../../english/template/debian/countries.wml:414
msgid "Vanuatu"
msgstr "وانواتو"

#: ../../english/template/debian/countries.wml:417
msgid "South Africa"
msgstr "آفریقای جنوبی"

#: ../../english/template/debian/countries.wml:420
msgid "Zimbabwe"
msgstr "زیمبابوه"

#~ msgid "Great Britain"
#~ msgstr "بریتانیای کبیر"
