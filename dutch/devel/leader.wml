#use wml::debian::template title="De projectleider van Debian" BARETITLE="true"
#use wml::debian::translation-check translation="c6d6679a692493cb18a61634dd830b498c3306bd"

#include "$(ENGLISHDIR)/devel/leader.data"

<p>De projectleider van Debian (DPL) is de officiële vertegenwoordiger
van het Debian Project. Deze persoon vervult twee belangrijke functies,
een interne en een externe.</p>

<p>In de externe functie vertegenwoordigt de projectleider het Debian
Project bij anderen. Dit omvat het geven van toespraken en presentaties over
Debian en het deelnemen aan handelsbeurzen. Ook het opbouwen van goede
relaties met andere organisaties en bedrijven valt hieronder.</p>

<p>Intern beheert de projectleider het project en bepaalt de visie ervan.
De projectleider spreekt met andere ontwikkelaars van Debian, in het bijzonder
met de gevolmachtigden om na te gaan hoe deze bij hun werk bijgestaan kunnen
worden. Om die reden bestaat een belangrijke taak van de projectleider uit
coördineren en communiceren.</p>


<h2>Aanstelling</h2>

<p>De projectleider wordt verkozen in een verkiezing waarin alle ontwikkelaars
van Debian stemrecht hebben. De ambtsperiode van de projectleider bedraagt
één jaar. Zes weken voor het vacant worden van de functie van projectleider,
brengt de <a href="secretary">Projectsecretaris</a> een nieuw verkiezingsproces
op gang. Tijdens de eerste week kan elke ontwikkelaar van Debian zich kandidaat
stellen voor de functie door zichzelf te nomineren. De drie daaropvolgende
weken worden gebruikt voor campagnevoering. Elke kandidaat maakt zijn programma
bekend en iedereen kan vragen stellen aan één of aan alle kandidaten. De
laatste twee weken is voorbehouden voor de stemperiode waarin de ontwikkelaars
hun stem kunnen uitbrengen.</p>

<p>Meer informatie over de projectleidersverkiezingen is te vinden op
<a href="../vote/">de verkiezingspagina's</a>.</p>


<h2>Taken die door de projectleider verricht worden</h2>

<h3>Gevolmachtigden aanstellen en te nemen beslissingen delegeren naar het
    <a href="tech-ctte">Technisch Comité</a></h3>

<p>De projectleider kan een specifieke verantwoordelijkheid definiëren en deze
delegeren naar een ontwikkelaar van Debian.</p>

<h3>Andere ontwikkelaars gezag verlenen</h3>

<p>De projectleider kan uitspraken doen ter ondersteuning van bepaalde
gezichtspunten of van andere projectleden.</p>

<h3>Besluiten nemen die een dringende actie vereisen</h3>

<h3>Besluiten nemen waarvoor niemand anders bevoegd is</h3>

<h3>In overleg met de ontwikkelaars beslissingen nemen in verband met
bezittingen voor doeleinden die verband houden met Debian</h3>

<p>De projectleider kan beslissingen nemen over hoe geld van Debian gebruikt
moet worden.</p>


<h2>Contactinformatie</h2>

<p>U kunt de Debian projectleider contacteren door een e-mail te
sturen in het Engels naar <email "leader@debian.org">.</p>

<h1>Over onze huidige leider</h1>

<p>De huidige projectleider van Debian is <current_leader>.</p>

<h2>Eerdere projectleiders</h2>

<p>Een volledige lijst van de vorige projectleiders is te vinden in de
<a href="$(DOC)/manuals/project-history/ch-leaders">Geschiedenis van het
Debian Project</a>.</p>
