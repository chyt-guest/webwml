<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>With the previous upload of the isc-dhcp package to Debian Squeeze LTS
two issues got introduced into LTS that are resolved by this upload.</p>

<ul>

<li>(1)

<p><a href="https://security-tracker.debian.org/tracker/CVE-2015-8605">CVE-2015-8605</a> 
had only been resolved for the LDAP variant of the DHCP
server package built from the isc-dhcp source package. With upload of
version 4.1.1-P1-15+squeeze10, now all DHCP server variants (LDAP and
non-LDAP alike) include the fix for <a href="https://security-tracker.debian.org/tracker/CVE-2015-8605">CVE-2015-8605</a>. Thanks to Ben
Hutchings for spotting this inaccuracy.</p></li>

<li>(2)

<p>The amd64 binary build of the previously uploaded isc-dhcp version
(4.1.1-P1-15+squeeze9) was flawed and searched for the dhcpd.conf
configuration file at the wrong location [1,2,3]. This flaw in the amd64
build had been caused by a not-100%-pure-squeeze-lts build system on the
maintainer's end. The amd64 build of version 4.1.1-P1-15+squeeze10 has
been redone in a brand-new build environment and does not show the
reported symptom(s) anymore.</p>

<p>I deeply apologize for the experienced inconvenience to all who
encountered this issue.</p></li>

</ul>

<p>[1] <a href="https://bugs.debian.org/811097">https://bugs.debian.org/811097</a><br>
[2] <a href="https://bugs.debian.org/811397">https://bugs.debian.org/811397</a><br>
[3] <a href="https://bugs.debian.org/811402">https://bugs.debian.org/811402</a></p>

<p>For Debian 6 <q>Squeeze</q>, these issues have been fixed in isc-dhcp version 4.1.1-P1-15+squeeze10</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-385-2.data"
# $Id: $
