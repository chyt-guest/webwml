<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several vulnerabilities were discovered in LibreOffice, the office
productivity suite.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9848">CVE-2019-9848</a>

    <p>Nils Emmerich discovered that malicious documents could execute
    arbitrary Python code via LibreLogo.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9849">CVE-2019-9849</a>

    <p>Matei Badanoiu discovered that the stealth mode did not apply to
    bullet graphics.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9850">CVE-2019-9850</a>

    <p>It was discovered that the protections implemented in <a href="https://security-tracker.debian.org/tracker/CVE-2019-9848">CVE-2019-9848</a>
    could be bypassed because of insufficient URL validation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9851">CVE-2019-9851</a>

    <p>Gabriel Masei discovered that malicious documents could execute
    arbitrary pre-installed scripts.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9852">CVE-2019-9852</a>

    <p>Nils Emmerich discovered that the protection implemented to address
    <a href="https://security-tracker.debian.org/tracker/CVE-2018-16858">CVE-2018-16858</a> could be bypassed by a URL encoding attack.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9853">CVE-2019-9853</a>

    <p>Nils Emmerich discovered that malicious documents could bypass
    document security settings to execute macros contained within the
    document.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9854">CVE-2019-9854</a>

    <p>It was discovered that the protection implemented to address
    <a href="https://security-tracker.debian.org/tracker/CVE-2019-9852">CVE-2019-9852</a> could be bypassed because of insufficient input
    sanitization.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1:4.3.3-2+deb8u13.</p>

<p>We recommend that you upgrade your libreoffice packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1947.data"
# $Id: $
