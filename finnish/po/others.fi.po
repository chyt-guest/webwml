msgid ""
msgstr ""
"Project-Id-Version: debian-webwml\n"
"PO-Revision-Date: 2016-05-22 22:26+0300\n"
"Last-Translator: Tommi Vainikainen <tvainika@debian.org>\n"
"Language-Team: Finnish <debian-l10n-finnish@lists.debian.org>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/banners/index.tags:7
msgid "Download"
msgstr "Imuroi"

#: ../../english/banners/index.tags:11
msgid "Old banner ads"
msgstr "Vanhoja mainospalkkeja"

#: ../../english/devel/debian-installer/ports-status.defs:10
msgid "Working"
msgstr "Työn alla"

#: ../../english/devel/debian-installer/ports-status.defs:20
msgid "sarge"
msgstr "sarge"

#: ../../english/devel/debian-installer/ports-status.defs:30
msgid "sarge (broken)"
msgstr "sarge (rikki)"

#: ../../english/devel/debian-installer/ports-status.defs:40
msgid "Booting"
msgstr "Käynnistetään"

#: ../../english/devel/debian-installer/ports-status.defs:50
msgid "Building"
msgstr "Käännetään"

#: ../../english/devel/debian-installer/ports-status.defs:56
msgid "Not yet"
msgstr "Ei vielä"

#: ../../english/devel/debian-installer/ports-status.defs:59
msgid "No kernel"
msgstr "Ei ydintä"

#: ../../english/devel/debian-installer/ports-status.defs:62
msgid "No images"
msgstr "Ei vedoksia"

#: ../../english/devel/debian-installer/ports-status.defs:65
msgid "<void id=\"d-i\" />Unknown"
msgstr "<void id=\"d-i\" />Tuntematon"

#: ../../english/devel/debian-installer/ports-status.defs:68
msgid "Unavailable"
msgstr "Ei saatavilla"

#: ../../english/devel/join/nm-steps.inc:7
msgid "New Members Corner"
msgstr "Uuden jäsenen nurkkaus"

#: ../../english/devel/join/nm-steps.inc:10
msgid "Step 1"
msgstr "Vaihe 1"

#: ../../english/devel/join/nm-steps.inc:11
msgid "Step 2"
msgstr "Vaihe 2"

#: ../../english/devel/join/nm-steps.inc:12
msgid "Step 3"
msgstr "Vaihe 3"

#: ../../english/devel/join/nm-steps.inc:13
msgid "Step 4"
msgstr "Vaihe 4"

#: ../../english/devel/join/nm-steps.inc:14
msgid "Step 5"
msgstr "Vaihe 5"

#: ../../english/devel/join/nm-steps.inc:15
msgid "Step 6"
msgstr "Vaihe 6"

#: ../../english/devel/join/nm-steps.inc:16
msgid "Step 7"
msgstr "Vaihe 7"

#: ../../english/devel/join/nm-steps.inc:19
msgid "Applicants' checklist"
msgstr "Hakijan muistilista"

#: ../../english/devel/website/tc.data:11
msgid ""
"See <a href=\"m4_HOME/intl/french/\">https://www.debian.org/intl/french/</a> "
"(only available in French) for more information."
msgstr ""
"Katso sivulta <a href=\"m4_HOME/intl/french/\">https://www.debian.org/intl/"
"french/</a> (vain ranskaksi) lisätietoa."

#: ../../english/devel/website/tc.data:12
#: ../../english/devel/website/tc.data:14
#: ../../english/devel/website/tc.data:15
#: ../../english/devel/website/tc.data:16
msgid "More information"
msgstr "Lisätietoja"

#: ../../english/devel/website/tc.data:13
msgid ""
"See <a href=\"m4_HOME/intl/spanish/\">https://www.debian.org/intl/spanish/</"
"a> (only available in Spanish) for more information."
msgstr ""
"Katso sivulta <a href=\"m4_HOME/intl/spanish/\">https://www.debian.org/intl/"
"spanish/</a> (saatavilla vain espanjaksi) lisätietoa."

#: ../../english/distrib/pre-installed.defs:18
msgid "Phone"
msgstr "Puhelin"

#: ../../english/distrib/pre-installed.defs:19
msgid "Fax"
msgstr "Faksi"

#: ../../english/distrib/pre-installed.defs:21
msgid "Address"
msgstr "Osoite"

#: ../../english/events/merchandise.def:10
msgid "Products"
msgstr "Tuotteet"

#: ../../english/events/merchandise.def:13
msgid "T-shirts"
msgstr "t-paitoja"

#: ../../english/events/merchandise.def:16
msgid "hats"
msgstr "hattuja"

#: ../../english/events/merchandise.def:19
msgid "stickers"
msgstr "tarroja"

#: ../../english/events/merchandise.def:22
msgid "mugs"
msgstr "mukeja"

#: ../../english/events/merchandise.def:25
msgid "other clothing"
msgstr "muita vaatteita"

#: ../../english/events/merchandise.def:28
msgid "polo shirts"
msgstr "poolopaitoja"

#: ../../english/events/merchandise.def:31
msgid "frisbees"
msgstr "frisbiit"

#: ../../english/events/merchandise.def:34
msgid "mouse pads"
msgstr "hiirimattoja"

#: ../../english/events/merchandise.def:37
msgid "badges"
msgstr "pinssejä"

#: ../../english/events/merchandise.def:40
msgid "basketball goals"
msgstr "koripallotelineitä"

#: ../../english/events/merchandise.def:44
msgid "earrings"
msgstr "korvarenkaita"

#: ../../english/events/merchandise.def:47
msgid "suitcases"
msgstr "matkalaukkuja"

#: ../../english/events/merchandise.def:50
msgid "umbrellas"
msgstr "sateenvarjoja"

#: ../../english/events/merchandise.def:53
msgid "pillowcases"
msgstr "tyynyliinoja"

#: ../../english/events/merchandise.def:56
msgid "keychains"
msgstr "avainketjut"

#: ../../english/events/merchandise.def:59
msgid "Swiss army knives"
msgstr "sveitsiläiset linkkuveitset"

#: ../../english/events/merchandise.def:62
msgid "USB-Sticks"
msgstr "USB-tikut"

#: ../../english/events/merchandise.def:77
msgid "lanyards"
msgstr ""

#: ../../english/events/merchandise.def:80
msgid "others"
msgstr ""

#: ../../english/events/merchandise.def:101
msgid "Donates money to Debian"
msgstr ""

#: ../../english/events/merchandise.def:106
msgid "Money is used to organize local free software events"
msgstr ""

#: ../../english/logos/index.data:6
msgid "With&nbsp;``Debian''"
msgstr "Mukana&nbsp;\"Debian\""

#: ../../english/logos/index.data:9
msgid "Without&nbsp;``Debian''"
msgstr "Ilman&nbsp;\"Debian\":ia"

#: ../../english/logos/index.data:12
msgid "Encapsulated PostScript"
msgstr "Kapseloitu PostScript"

#: ../../english/logos/index.data:18
msgid "[Powered by Debian]"
msgstr "[Käytössä Debian]"

#: ../../english/logos/index.data:21
msgid "[Powered by Debian GNU/Linux]"
msgstr "[Käytössä Debian GNU/Linux]"

#: ../../english/logos/index.data:24
msgid "[Debian powered]"
msgstr "[Debian-käyttöinen]"

#: ../../english/logos/index.data:27
msgid "[Debian] (mini button)"
msgstr "[Debian] (pieni logo)"

#: ../../english/mirror/submit.inc:7
msgid "same as the above"
msgstr "sama kuin yllä"

#: ../../english/women/profiles/profiles.def:24
msgid "How long have you been using Debian?"
msgstr "Kuinka pitkään olet käyttänyt Debiania?"

#: ../../english/women/profiles/profiles.def:27
msgid "Are you a Debian Developer?"
msgstr "Oletko Debian-kehittäjä?"

#: ../../english/women/profiles/profiles.def:30
msgid "What areas of Debian are you involved in?"
msgstr "Millä Debianin aloilla olet osallisena?"

#: ../../english/women/profiles/profiles.def:33
msgid "What got you interested in working with Debian?"
msgstr "Mikä sai sinut kiinnostumaan työskentelemään Debianin kanssa?"

#: ../../english/women/profiles/profiles.def:36
msgid ""
"Do you have any tips for women interested in getting more involved with "
"Debian?"
msgstr ""
"Onko sinulla vinkkejä naisille, jotka ovat kiinnostuneet osallistumaan "
"enemmän Debianiin?"

#: ../../english/women/profiles/profiles.def:39
msgid ""
"Are you involved with any other women in technology group? Which one(s)?"
msgstr ""
"Oletko tekemisissä minkään muun teknologia-alan naisten ryhmän kanssa? Minkä?"

#: ../../english/women/profiles/profiles.def:42
msgid "A bit more about you..."
msgstr "Vielä vähän lisää sinusta..."

#: ../../english/y2k/l10n.data:6
msgid "OK"
msgstr "OK"

#: ../../english/y2k/l10n.data:9
msgid "BAD"
msgstr "HUONO"

#: ../../english/y2k/l10n.data:12
msgid "OK?"
msgstr "OK?"

#: ../../english/y2k/l10n.data:15
msgid "BAD?"
msgstr "HUONO?"

#: ../../english/y2k/l10n.data:18
msgid "??"
msgstr "??"

#: ../../english/y2k/l10n.data:21
msgid "Unknown"
msgstr "Tuntematon"

#: ../../english/y2k/l10n.data:24
msgid "ALL"
msgstr "KAIKKI"

#: ../../english/y2k/l10n.data:27
msgid "Package"
msgstr "Paketti"

#: ../../english/y2k/l10n.data:30
msgid "Status"
msgstr "Tila"

#: ../../english/y2k/l10n.data:33
msgid "Version"
msgstr "Versio"

#: ../../english/y2k/l10n.data:36
msgid "URL"
msgstr "URL"

#~ msgid "Wanted:"
#~ msgstr "Hinnat:"

#~ msgid "Who:"
#~ msgstr "Kuka:"

#~ msgid "Architecture:"
#~ msgstr "Arkkitehtuuri:"

#~ msgid "Specifications:"
#~ msgstr "Tiedot:"

#~ msgid "Where:"
#~ msgstr "Missä:"

#~ msgid "p<get-var page />"
#~ msgstr "s. <get-var page />"

#~ msgid "Previous Talks:"
#~ msgstr "Aiemmat puheet:"

#~ msgid "Languages:"
#~ msgstr "Kielet:"

#~ msgid "Location:"
#~ msgstr "Sijainti:"

#~ msgid "Topics:"
#~ msgstr "Aiheet:"

#~ msgid "beta 4"
#~ msgstr "beta 4"

#~ msgid "rc2"
#~ msgstr "rc2"

#~ msgid "rc3"
#~ msgstr "rc3"

#~ msgid "rc3 (broken)"
#~ msgstr "rc3 (rikki)"

#~ msgid "closed"
#~ msgstr "suljettu"

#~ msgid "open"
#~ msgstr "avoin"

#~ msgid "Unsubscribe"
#~ msgstr "Poistu"

#~ msgid "Please select which lists you want to unsubscribe from:"
#~ msgstr "Valitse listat joilta haluat pois:"

#~ msgid ""
#~ "See the <a href=\"./#subunsub\">mailing lists</a> page for information on "
#~ "how to unsubscribe using e-mail. An <a href=\"subscribe\">subscription "
#~ "web form</a> is also available, for subscribing to mailing lists. "
#~ msgstr ""
#~ "Katso <a href=\"./#subunsub\">postilista</a> -sivulta kuinka poistut "
#~ "sähköpostitse.  Listoille voi <a href=\"subscribe\">liittyä täyttämällä "
#~ "www-lomakkeen</a>. "

#~ msgid "Mailing List Unsubscription"
#~ msgstr "Postilistoilta poistuminen"

#~ msgid ""
#~ "Please respect the <a href=\"./#ads\">Debian mailing list advertising "
#~ "policy</a>."
#~ msgstr ""
#~ "Muistathan noudattaa <a href=\"./#ads\">Debianin postilistojen "
#~ "mainostuslinjausta</a>."

#~ msgid "Clear"
#~ msgstr "Tyhjennä"

#~ msgid "Subscribe"
#~ msgstr "Liity"

#~ msgid "Your E-Mail address:"
#~ msgstr "Sähköpostiosoitteesi:"

#~ msgid "is a read-only, digestified version."
#~ msgstr "on ainoastaan luettavaksi, kokoelmaversio."

#~ msgid "Subscription:"
#~ msgstr "Liittyminen:"

#~ msgid ""
#~ "Only messages signed by a Debian developer will be accepted by this list."
#~ msgstr ""
#~ "Ainoastaan Debian-kehittäjän allekirjoittamat viestit hyväksytään "
#~ "listalle."

#~ msgid "Posting messages allowed only to subscribers."
#~ msgstr "Viestien lähettäminen sallittu vain listalle liittyneille."

#~ msgid "Moderated:"
#~ msgstr "Moderoitu:"

#~ msgid "No description given"
#~ msgstr "Kuvausta ei annettu"

#~ msgid "Please select which lists you want to subscribe to:"
#~ msgstr "Valitse listat joille haluat liittyä:"

#~ msgid ""
#~ "Note that most Debian mailing lists are public forums. Any mails sent to "
#~ "them will be published in public mailing list archives and indexed by "
#~ "search engines. You should only subscribe to Debian mailing lists using "
#~ "an e-mail address that you do not mind being made public."
#~ msgstr ""
#~ "Huomaa, että useimmat Debianin postilistoista ovat julkisia foorumeita. "
#~ "Kaikki niille lähetetyt sähköpostit julkaistaan julkisessa postilista-"
#~ "arkistossa ja hakukoneet indeksoivat ne. Liittyessäsi Debianin "
#~ "postilistoille käytä vain sellaista sähköpostiosoitetta jonka "
#~ "julkaisemisen hyväksyt."

#~ msgid ""
#~ "See the <a href=\"./#subunsub\">mailing lists</a> page for information on "
#~ "how to subscribe using e-mail. An <a href=\"unsubscribe\">unsubscription "
#~ "web form</a> is also available, for unsubscribing from mailing lists. "
#~ msgstr ""
#~ "Katso <a href=\"./#subunsub\">postilista</a> -sivulta kuinka liityt "
#~ "sähköpostitse.  Listoilta voi <a href=\"unsubscribe\">poistua täyttämällä "
#~ "www-lomakkeen</a>. "

#~ msgid "Mailing List Subscription"
#~ msgstr "Postilistoille liittyminen"

#~ msgid ""
#~ "<total_consultant> Debian consultants listed in <total_country> countries "
#~ "worldwide."
#~ msgstr ""
#~ "Listattu yhteensä <total_consultant> Debian-konsulttia <total_country> "
#~ "maassa maailmanlaajuisesti."

#~ msgid "Willing to Relocate"
#~ msgstr "Valmis muuttamaan"

#~ msgid "Rates:"
#~ msgstr "Hinnat:"

#~ msgid "Email:"
#~ msgstr "Sähköposti:"

#~ msgid "or"
#~ msgstr "tai"

#~ msgid "URL:"
#~ msgstr "WWW-sivut:"

#~ msgid "Company:"
#~ msgstr "Yritys:"

#~ msgid "Name:"
#~ msgstr "Nimi:"
