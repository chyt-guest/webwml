#use wml::debian::template title="Debian voor PowerPC" NOHEADER="yes"
#include "$(ENGLISHDIR)/ports/powerpc/menu.inc"
#use wml::debian::toc
#use wml::debian::translation-check translation="bd64e53e8b270cd2977b613f14396ecff58deca8"

<toc-display/>

<toc-add-entry name="about">Debian voor PowerPC</toc-add-entry>

<p>
<img src="pics/ppc750.jpg" alt="PPC-750 picture" class="rightico">
 De PowerPC-architectuur is een architectuur met een <a href="http://en.wikipedia.org/wiki/RISC">\
 RISC</a> microprocessor, ontwikkeld door
<a href="http://www.ibm.com/systems/power/">IBM</a>,
 Motorola (tegenwoordig <a href="http://www.freescale.com/">Freescale</a>) en
 <a href="http://www.apple.com/">Apple</a>. De PowerPC-architectuur kent zowel
 64-bits als 32-bits toepassingen (in de 64-bits toepassing is ook
 de 32-bits toepassing inbegrepen). De eerste PowerPC-microprocessor was de
 601, een 32-bits toepassing die uitgebracht werd in 1992. Sindsdien werden
 verschillende andere 32-bits toepassingen uitgebracht, waaronder de
 603, 604, 750 (G3), 7400 (G4) en de ingebedde PowerQUICC
 communicatieprocessor. Tot de 64-bits toepassingen behoren de 620, POWER4,
 POWER5 en de 970 (G5).
</p>

<p>
 Linux voor de PowerPC werd voor het eerst uitgebracht met versie 2.2.x van
 de kernel. Een belangrijke hulpbron voor de ontwikkeling van Linux voor
 PowerPC is <a href="http://penguinppc.org/">penguinppc</a>, waar ook een
 lijst met compatibele hardware te vinden is. Ondersteuning voor PowerPC in de
 Linux kernel wordt nu verder ontwikkeld als onderdeel van de `algemene'
 ontwikkeling van de Linux kernel op <a href="http://kernel.org">kernel.org</a>.
</p>

<p>
 Het geschikt maken van Debian voor PowerPC startte in 1997 op het <a href="http://www.linux-kongress.org/">
 Duitse Linux-congres</a> in W&uuml;rzburg.
 Er werd <a href="http://www.infodrom.north.de/Infodrom/tervola.html">een
 PowerPC-machine</a> (Motorola StarMax 4000, 200 MHz 604e) geschonken aan
 het Debian project. Meer informatie over deze computer is te vinden op de
 <a href="history">pagina met wat geschiedenis</a>.
</p>

<toc-add-entry name="powerpc">Debian op 32-bits PowerPC (powerpc)</toc-add-entry>

<p>
Dit werd voor het eerst een officiële <q>release-architectuur</q> met Debian
GNU/Linux 2.2 (<q>potato</q>) en ze behield deze status tot de uitgave van
Debian 9 (<q>stretch</q>). De laatste release met ondersteuning voor 32-bits
PowerPC is Debian 8 (<q>jessie</q>).

Raadpleeg de <a href="$(HOME)/releases/jessie/powerpc/release-notes/">\
notities bij de release</a> en de <a href="$(HOME)/releases/jessie/powerpc/">\
installatiehandleiding</a> voor bijkomende informatie.
</p>

<toc-add-entry name="ppc64el">Debian op 64-bits Little Endian PowerPC (ppc64el)</toc-add-entry>
<p>
Sinds de release van Debian 8 (<q>jessie</q>), is ppc64el een officieel
door Debian ondersteunde architectuur.

Raadpleeg de <a href="$(HOME)/releases/stable/ppc64el/release-notes/">\
notities bij de release</a> en de <a href="$(HOME)/releases/stable/ppc64el/">\
installatiehandleiding</a>.
</p>

<p>
Hier vindt u informatie over de
<a href="http://en.wikipedia.org/wiki/Ppc64">64-bits PowerPC</a>
<a href="http://en.wikipedia.org/wiki/Little_endian">Little Endian</a>
architectuur.
</p>
<p>
Merk evenwel op dat er ook informatie te vinden is op de
<a href="https://wiki.debian.org/ppc64el">ppc64el wiki</a>-pagina, zoals
informatie over de installatie en over de ABI.
</p>

<toc-add-entry name="installation">Installatie</toc-add-entry>

<p>
 Er bestaat een variëteit van systemen de gebruik maken van de PowerPC
 microprocessor. Raadpleeg onze <a href="inst/install">Installatie</a>pagina's
 voor specifieke informatie over het installeren van Debian/PowerPC op uw
 systeem.
</p>


<p>
 U moet zich ook bewust zijn van enkele eigenaardigheden bij de installatie
 op een iBook, een TiBook, of een iBook2, in het bijzonder als u behalve
 met Linux ook met Mac OS X wilt kunnen opstarten (dual boot). Bepaalde
 iBook2-hardware, in het bijzonder op recent uitgebrachte modellen,
 wordt momenteel nog niet goed ondersteund. Voor specifieke informatie over
 mogelijke problemen en oplossingen, kunt u de volgende webpagina's
 bestuderen:
</p>

<ul>
<li>William R. Sowerbutts' <a
         href="http://www.sowerbutts.com/linux-mac-mini/">Installing
         Debian GNU/Linux on the Mac Mini</a></li>
<li>Mij's <a href="http://mij.oltrelinux.com/ibook/ppc_linux.html">\
       Hints on installing debian on an iBook2</a></li>
<li><a href="http://seb.france.free.fr/linux/ibookG4/iBookG4-howto.html">\
       Installing Debian GNU/Linux on an iBook 3.3 (G4)</a> door Sébastien FRANÇOIS</li>
<li><a href="https://lists.debian.org/debian-powerpc/2002/07/msg00858.html">\
      Debian network installation on IBM RS/6000 44P-170 (POWER3)
      HOWTO</a> door Rolf Brudeseth</li>
<li> Installing Debian GNU/Linux on a p630 LPAR (7028-6C4) - <a
     href="https://people.debian.org/~fmw/p630-LPAR-Debian-en.txt">Engels</a>
     en <a
     href="https://people.debian.org/~fmw/p630-LPAR-Debian-de.txt">Duits</a>
     (Florian M. Weps)</li>
<li>Daniel DeVoto's <a href="http://ppcluddite.blogspot.de/2012/03/installing-debian-linux-on-ppc-part-i.html">\
Installing Debian Wheezy/testing on an iBook G3</a></li>
</ul>

<p>
 Debian GNU/Linux biedt geen officiële ondersteuning voor NuBus PowerPC
 machines, zoals de 6100, 7100, 8100 en het grootste deel van de Performa
 serie. Er is echter een op MkLinux gebaseerde kernel beschikbaar die vanaf
 een Mac OS opstartlader opgestart kan worden. Er kan een Debian systeem
 geïnstalleerd worden met deze kernel, welke te vinden is op
 <url "http://nubus-pmac.sourceforge.net/">.
</p>

<p>
 Bij het opwaarderen van Potato naar Woody of bij het opwaarderen van zeer
 oude kernels, moet u op de hoogte zijn van <a href="keycodes">belangrijke
 informatie</a> in verband met een gewijzigde toetsenbordcodering. Hiermee
 kunt u tijd winnen en uzelf wat hoofdbrekens besparen!
</p>

<toc-add-entry name="docs">Links - Documentatie</toc-add-entry>

<p>Actuele informatie over Linux voor PowerPC is te vinden op
<a href="http://penguinppc.org/">PenguinPPC</a>.
We hebben een <a href="docu">collectie oudere PowerPC-links</a> en
enkele <a href="devel">aanwijzingen</a> voor Debian PowerPC ontwikkelaars.</p>

<p>Hardware-specificaties van Apple computers zijn te vinden op
<a href="http://support.apple.com/specs/">AppleSpec</a>.</p>
<p>
Meer informatie en documentatie over de POWER-architectuur:
</p>
<ul>
<li><a href="https://www-03.ibm.com/technologyconnect/tgcm/TGCMServlet.wss?alias=OpenPOWER">
Officiële ELFv2 ABI-specificatie</a> (onder de sectie 'Link')
of deze
<a href="https://www-03.ibm.com/technologyconnect/tgcm/TGCMFileServlet.wss/ABI64BitOpenPOWER_21July2014_pub.pdf?id=B81AEC1A37F5DAF185257C3E004E8845">
PDF</a>. Registratie vereist. </li>
<li><a href="https://gcc.gnu.org/wiki/cauldron2014#Slides_and_Notes">GNU Tools
Caldron 2014</a> dia's &amp; video</li>
</ul>

<toc-add-entry name="availablehw">Hardware die ter beschikking staat van
ontwikkelaars</toc-add-entry>

<p>Voor toegang tot de powerpc/ppc64el Debian porterboxen raadpleegt u de
<a href="https://db.debian.org/machines.cgi">lijst met Debian-machines</a>.
</p>

<toc-add-entry name="contact">Contactinformatie</toc-add-entry>

<p>Indien u hulp nodig heeft, kunt u die op de volgende plaatsen vragen:</p>

<h3>Mailinglijsten</h3>

<p>De Debian PowerPC mailinglijst is de geëigende plaats voor vragen,
suggesties voor verbeteringen, of om gewoon te praten over PowerPC-systemen
met Debian. Ook de mailinglijsten debian-user (Engels) en debian-user-dutch
(Nederlands) kunnen gebruikt worden voor algemene vragen over Debian die niet
specifiek over PowerPC gaan.</p>

<p>Om in te tekenen op de lijst stuurt u een e-mail naar
<a href="mailto:debian-powerpc-request@lists.debian.org">
&lt;debian-powerpc-request@lists.debian.org&gt;</a> met het woord
"subscribe" als onderwerp.
<a href="https://lists.debian.org/debian-powerpc/">Er bestaan archieven
van de mailinglijst</a>.</p>

<h3>Usenet-nieuwsgroepen</h3>

<p>Linux voor PowerPC-systemen</p>
<ul>
  <li><a href="news:comp.os.linux.powerpc">comp.os.linux.powerpc</a></li>
</ul>

<p>Linux in het algemeen</p>
<ul>
  <li><a href="news:comp.os.linux.misc">comp.os.linux.misc</a></li>
  <li><a href="news:comp.os.linux.networking">comp.os.linux.networking</a></li>
  <li><a href="news:comp.os.linux.hardware">comp.os.linux.hardware</a></li>
  <li><a href="news:comp.os.linux.x">comp.os.linux.x</a></li>
</ul>

<h3>IRC</h3>

<p>De kanalen <code>#debian</code> (Engels) en <code>#debian-nl</code>
(Nederlands) op <code>irc.debian.org</code> zijn bedoeld
voor algemene zaken in verband met Debian. Er is ook het kanaal
<code>#debianppc</code> dat bedoeld is voor zaken die specifiek verband
houden met Debian op PowerPC-processors. U zult praktisch altijd iemand
online vinden die met u informatie wil uitwisselen en u graag helpt met uw
probleem.</p>

<hr />

#include "$(ENGLISHDIR)/ports/powerpc/menu.inc"
