<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-7033">CVE-2018-7033</a>
      <p>Fix for issue in accounting_storage/mysql plugin by always
      escaping strings within the slurmdbd.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10995">CVE-2018-10995</a>

      <p>Fix for mishandling of user names (aka user_name fields) and
      group ids (aka gid fields).</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
14.03.9-5+deb8u3.</p>

<p>We recommend that you upgrade your slurm-llnl packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1437.data"
# $Id: $
