<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Alberto Garcia, Francisco Oca and Suleman Ali of Offensive Research
discovered that the Xerces-C XML parser mishandles certain kinds of
external DTD references, resulting in dereference of a NULL pointer
while processing the path to the DTD. The bug allows for a denial of
service attack in applications that allow DTD processing and do not
prevent external DTD usage, and could conceivably result in remote code
execution.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
3.1.1-3+deb7u5.</p>

<p>We recommend that you upgrade your xerces-c packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1328.data"
# $Id: $
