#use wml::debian::translation-check translation="1fe88c119d23384c59ccd2398be9feb00895a4a3" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Deux vulnérabilités de sécurité ont été trouvées dans OpenSSL, la boîte à
outils associée à SSL (Secure Socket Layer).</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-1547">CVE-2019-1547</a>

<p>Normalement dans OpenSSL les groupes EC ont toujours un cofacteur
présent et cela est utilisé pour les chemins au code immunisé contre les
canaux auxiliaires. Cependant, dans certains cas, il est possible de
construire un groupe utilisant des paramètres explicites (au lieu
d’utiliser une courbe nommée). Dans ces cas là, il est possible qu’un tel
groupe n’ait pas de cofacteur présent. Cela peut arriver même si tous les
paramètres correspondent à une courbe nommée connue. Si une telle courbe
est utilisée, alors OpenSSL se retrouve avec des chemins dont le code n’est
pas immunisé contre les canaux auxiliaires. Cela peut aboutir à la
récupération entière de la clef lors des opérations de signature ECDSA.
Pour être vulnérable, un attaquant devrait avoir la possibilité de
chronométrer la création d’un grand nombre de signatures où des paramètres
explicites sans cofacteur présent sont utilisés par une application
utilisant libcrypto. Pour éviter toute ambigüité, libssl n’est pas
vulnérable parce que les paramètres explicites ne sont jamais utilisés.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-1563">CVE-2019-1563</a>

<p>Dans les situations où un attaquant reçoit une notification automatique
de la réussite ou l’échec d’un essai de déchiffrement, un attaquant, après
l’envoi d’un très grand nombre de messages à déchiffrer, peut récupérer
une clef de chiffrement délivrée en CMS ou PKCS7 ou déchiffrer n’importe quel
message de chiffrement RSA qui était chiffré avec la clef RSA publique,
en utilisant une attaque d’oracle par remplissage Bleichenbacher. Les
applications ne sont pas affectées si elles utilisent un certificat de
concert avec la clef RSA privée pour les fonctions CMS_decrypt ou
PKCS7_decrypt pour choisir l’information du destinataire à déchiffrer.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 1.0.1t-1+deb8u12.</p>
<p>Nous vous recommandons de mettre à jour vos paquets openssl.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1932.data"
# $Id: $
