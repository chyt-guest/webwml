#use wml::debian::translation-check translation="ec63203ae7c88b70ad002f6a6d89bd22297d4fb8" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Quelques vulnérabilités ont été découvertes dans ghostscript, un interpréteur
langage PostScript et pour PDF.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19134">CVE-2018-19134</a>

<p>L’opérateur setpattern ne vérifiait pas correctement certains types. Un
document PostScript spécialement contrefait pourrait exploiter cela pour planter
Ghostscript ou, éventuellement, exécuter du code arbitraire dans le contexte du
processus Ghostscript. C’est un problème de confusion de type à cause d’un
défaut de vérification que l’implémentation du dictionnaire de modèles était de
type structure.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19478">CVE-2018-19478</a>

<p>L’ouverture d’un fichier PDF soigneusement contrefait aboutissait à des
calculs très longs. Un arbre de pages suffisamment nocif peut conduire à des
montants très importants de temps de vérification d’arbre lors de la récursion.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans la version 9.06~dfsg-2+deb8u13.</p>

<p>Nous vous recommandons de mettre à jour vos paquets ghostscript.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1620.data"
# $Id: $
