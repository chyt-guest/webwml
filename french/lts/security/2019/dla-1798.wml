#use wml::debian::translation-check translation="4e0dd0a490060cad85a8585dae794028f504fe7d" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Un problème de « Polymorphic Typing » a été découvert dans jackson-databind,
une bibliothèque JSON pour Java. Lorsque Typing par défaut est activé (soit
globalement ou pour une propriété spécifique) pour un point d’entrée JSON exposé
externellement, le service possédait un fichier jar mysql-connector-java
(8.0.14 ou précédents) dans le classpath, et qu’un attaquant pouvait héberger un
serveur MySQL contrefait atteignable par la victime, un attaquant pouvait envoyer
un message JSON contrefait pouvant lui permettre de lire des fichiers locaux
arbitraires sur le serveur. Cela arrivait à cause d’une validation manquante de
com.mysql.cj.jdbc.admin.MiniAdmin.</p>


<p>Pour Debian 8 <q>Jessie</q>, ce problème a été corrigé dans
la version 2.4.2-2+deb8u6.</p>
<p>Nous vous recommandons de mettre à jour vos paquets jackson-databind.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1798.data"
# $Id: $
