<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Guido Vranken discovered that incorrect memory management in libtirpc,
a transport-independent RPC library used by rpcbind and other programs
may result in denial of service via memory exhaustion (depending on
memory management settings).</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
0.2.2-5+deb7u1.</p>

<p>We recommend that you upgrade your libtirpc packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-936.data"
# $Id: $
